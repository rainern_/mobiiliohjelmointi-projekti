#include <SPI.h>
#include <BLEPeripheral.h>
#include <TimerOne.h>

/*nrf8001 Pin layout:
CLK = 13 
MISO = 12 
MOSI = 11
REQ = 10
RDY = 2
RST = 9*/

// Led Pin Config
int LED = 5;

// Määritetään tarvittavat BLE pinnit
#define BLE_REQ   10
#define BLE_RDY   2     //Internupt Pin
#define BLE_RST   9

// create peripheral instance, see pinouts above
BLEPeripheral           blePeripheral               = BLEPeripheral(BLE_REQ, BLE_RDY, BLE_RST);

// create services
BLEService              ledService                  = BLEService("0000f00d1212efde1523785feabcd123");
BLEService              temperatureService          = BLEService("CCC0");
BLEService              humidityService             = BLEService("DDD0");
BLEService              updateService               = BLEService("53632a3cf0d54ceeb33c1b999b583f37");


// create led characteristics
BLECharCharacteristic   ledCharacteristic = BLECharCharacteristic("0000f00d1212efde1523785feabcd123", BLERead | BLEWrite);

// create temperature characteristic & descriptor
BLEFloatCharacteristic temperatureCharacteristic = BLEFloatCharacteristic("CCC1", BLERead | BLENotify);
BLEDescriptor temperatureDescriptor = BLEDescriptor("2901", "Temp Celsius");

// create humidity characteristic & descriptor
BLEFloatCharacteristic humidityCharacteristic = BLEFloatCharacteristic("DDD1", BLERead | BLENotify);
BLEDescriptor humidityDescriptor = BLEDescriptor("2901", "Humidity Percent");

// create update characteristics
BLECharCharacteristic   updateCharacteristic = BLECharCharacteristic("53632a3cf0d54ceeb33c1b999b583f37", BLEWrite);


//float muuttujat
float temp, humi;

//Boolean muuttuja
volatile bool updateTime = false;

void setup(void)
{ 
  //Serial open
  Serial.begin(9600);
  
  //Led asetetaan outputiksi 
  pinMode(LED, OUTPUT);


  //Annetaan lokaali nimi
  blePeripheral.setLocalName("BLE-UNO");

 //--------------------------------------------------------------------------------------
 // Led Advertisement & add service and characteristic
 
  blePeripheral.setAdvertisedServiceUuid(ledService.uuid());
  blePeripheral.addAttribute(ledService);
  blePeripheral.addAttribute(ledCharacteristic);

 //--------------------------------------------------------------------------------------
  // Temperature Advertisement & add service and characteristic

  blePeripheral.setAdvertisedServiceUuid(temperatureService.uuid());
  blePeripheral.addAttribute(temperatureService);
  blePeripheral.addAttribute(temperatureCharacteristic);
  blePeripheral.addAttribute(temperatureDescriptor);

 //--------------------------------------------------------------------------------------
  //Temperature Advertisement & add service and characteristic

  blePeripheral.setAdvertisedServiceUuid(humidityService.uuid());
  blePeripheral.addAttribute(humidityService);
  blePeripheral.addAttribute(humidityCharacteristic);
  blePeripheral.addAttribute(humidityDescriptor);

 //--------------------------------------------------------------------------------------
  //Update Advertisement & add service and characteristic
  
  blePeripheral.setAdvertisedServiceUuid(updateService.uuid());
  blePeripheral.addAttribute(updateService);
  blePeripheral.addAttribute(updateCharacteristic);

 //--------------------------------------------------------------------------------------


  // assign event handlers for connected, disconnected to peripheral
  blePeripheral.setEventHandler(BLEConnected, blePeripheralConnectHandler);
  blePeripheral.setEventHandler(BLEDisconnected, blePeripheralDisconnectHandler);

  // assign event handlers for characteristic
  ledCharacteristic.setEventHandler(BLEWritten, ledCharacteristicWritten);
  updateCharacteristic.setEventHandler(BLEWritten, updateCharacteristicWritten);


  // begin initialization
  blePeripheral.begin();

  //
  Timer1.initialize(2 * 6000000); // in milliseconds
  Timer1.attachInterrupt(timerHandler);

  Serial.println(F("BLE LED Peripheral"));
}

void loop()
{
    // poll peripheral
  blePeripheral.poll();

  switch (updateCharacteristic.value()) {
    case 1:
      if(updateTime == true){
        updateTemperature();
        updateHumidity();
        updateTime = false;
        break;
      }
      else{}
    default:
    break;
  }
}

void blePeripheralConnectHandler(BLECentral& central) {
  // central connected event handler
  Serial.print(F("Connected event, central: "));
  Serial.println(central.address());
}

void blePeripheralDisconnectHandler(BLECentral& central) {
  // central disconnected event handler
  Serial.print(F("Disconnected event, central: "));
  Serial.println(central.address());
}

void ledCharacteristicWritten(BLECentral& central, BLECharacteristic& characteristic) {
  // central wrote new value to characteristic, update LED
  Serial.print(F("Characteristic event, writen: "));

  switch (ledCharacteristic.value()) {
  case 0:
    Serial.println(F("LED off"));
    digitalWrite(LED, LOW);
    break;
  case 1:
    Serial.println(F("LED on"));
    digitalWrite(LED, HIGH);
    break;
  }
}

void updateCharacteristicWritten(BLECentral& central, BLECharacteristic& characteristic) {
  switch (updateCharacteristic.value()) {
    //Update Button
    case 0:
     temp = random(-3000, 3000) / 100.0;
     temperatureCharacteristic.setValue(temp);
     Serial.println(F("Temperature sent"));
     Serial.print(temperatureCharacteristic.value());

     humi = random(0, 10000) / 100.0;
     humidityCharacteristic.setValue(humi);
     Serial.println(F("Humidity sent"));
     Serial.print(humidityCharacteristic.value());
      break;
    default: //Notification
      break;
     }
}

void updateTemperature(){
     temp = random(-3000, 5000) / 100.0;
     temperatureCharacteristic.setValue(temp);
     Serial.println(F("Temperature sent"));
     Serial.print(temperatureCharacteristic.value());
}

void updateHumidity(){
     humi = random(0, 10000) / 100.0;
     humidityCharacteristic.setValue(humi);
     Serial.println(F("Humidity sent"));
     Serial.print(humidityCharacteristic.value());
}

void timerHandler() {
  updateTime = true;
}
