package com.example.bleclient;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.CompoundButton;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import static android.widget.Toast.LENGTH_SHORT;


public class MainActivity extends AppCompatActivity {

    //Firebase
    private DatabaseReference mDatabase;

    //Bluetooth Määritykset:
    BluetoothManager btManager;
    BluetoothAdapter btAdapter = null;
    BluetoothLeScanner btScanner;
    BluetoothGatt btGatt;

    //Bluetooth lupa muuttuja:
    private final static int REQUEST_ENABLE_BT = 1;

    //Määritetään Characteristic muuttujat:
    BluetoothGattCharacteristic ledSwitchCharacteristic;
    BluetoothGattCharacteristic temperatureCharacteristic;
    BluetoothGattCharacteristic humidityCharacteristic;
    BluetoothGattCharacteristic updateCharacteristic;


    //Määritetään Serives/palvelu tunnukset:
    private static final UUID UUID_ledService = UUID.fromString("0000f00d-1212-efde-1523-785feabcd123");
    private static final UUID UUID_temperatureService = UUID.fromString("0000ccc0-0000-1000-8000-00805f9b34fb");
    private static final UUID UUID_humidityService = UUID.fromString("0000ddd0-0000-1000-8000-00805f9b34fb");
    private static final UUID UUID_temperatureCharacteristic = UUID.fromString("0000ccc1-0000-1000-8000-00805f9b34fb");
    private static final UUID UUID_humidityCharacteristic = UUID.fromString("0000ddd1-0000-1000-8000-00805f9b34fb");
    private static final UUID UUID_updateService = UUID.fromString("53632a3c-f0d5-4cee-b33c-1b999b583f37");


    //Class muuttuja:
    DataInsert dataInsert;


    //UI & Muuttuja määritykset:
    Button connectButton;
    Button disconnectButton;
    Button updateButton;
    Button databaseButton;
    CompoundButton ledButton;
    CompoundButton notificationButton;
    TextView temperatureView;
    TextView humidityView;

    String adress;
    String deviceName = "";
    float value;
    float temperature;
    float humidity;
    int printTarget = 0;
    boolean ledStatus = false;
    boolean scanStatus = false;


    //Ohjeman aloitus:
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Firebase käyttöönotto:
        dataInsert = new DataInsert();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Measurements");

        //UI-Yhdistäminen
        connectButton = (Button) findViewById(R.id.connectButton);
        disconnectButton = (Button) findViewById(R.id.disconnectButton);
        updateButton = (Button) findViewById(R.id.updateButton);
        databaseButton = (Button) findViewById(R.id.databaseButton);
        temperatureView = (TextView) findViewById(R.id.temperatureView);
        humidityView = (TextView) findViewById(R.id.humidityView);

        //Led On/Off Napin Setup
        ledButton = (CompoundButton) findViewById(R.id.ledButton);
        ledButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                ledOn();
            } else {
                ledOff();
            }
        });

        //Notification napin Setup:
        notificationButton = (CompoundButton) findViewById(R.id.notificationButton);
        notificationButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                notificationOn();
            } else {
                notificationOff();
            }
        });


        //Bluetooth Adapter & Manager alustus:
        btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = btManager.getAdapter();

        //Pyydetään Bluetooth käyttö luvat:
        checkBtPermissions();

        //Laitetaan Bluetooth päälle:
        enableBt();


    }

    //-----BLE Aliohjelmia & Kutsuja----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //Tarkista Bluetooth luvat toiminto:
    public void checkBtPermissions() {
        this.requestPermissions(new String[]{
                Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_ENABLE_BT);
    }

    //Bluetooth enable toiminto:
    public void enableBt() {
        if (btAdapter == null) {
            Toast.makeText(getApplicationContext(), "Your device does not support Bluetooth", Toast.LENGTH_LONG).show();
        }
        if (!btAdapter.isEnabled()) {
            Intent enableBTIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBTIntent, REQUEST_ENABLE_BT);
        }
    }


    //onResume timinto:
    @Override
    protected void onResume() {
        super.onResume();
        if (btAdapter == null || !btAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);

        }
    }


    //Laiteen skannaus callback toiminto:
    private final ScanCallback leScanCallback = new ScanCallback() {
        @Override
        //Jos scannaus onnistuu:
        public void onScanResult(int callbackType, ScanResult result) {
            if (result.getDevice().getName().equals("BLE-UNO")) {
                Log.i("Scan Result", "BLE-UNO Found");
                adress = result.getDevice().getAddress();
                deviceName = result.getDevice().getName();
                stopScanning();
                scanStatus = false;
                connection();
            }
        }

        //Jos Scannaus epäonnistuu:
        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Log.i("BLE", "error");
        }
    };

    //Laitteen Ydistys callback toiminto:
    private final BluetoothGattCallback connectecallabck = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            btGatt = gatt;
            gatt.discoverServices();
        }

        //Löydettyjen servicejen toiminto:
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);

            //Led
            BluetoothGattService ledService = gatt.getService(UUID_ledService);
            ledSwitchCharacteristic = ledService.getCharacteristic(UUID_ledService);

            //Lämpötila
            BluetoothGattService temperatureService = gatt.getService(UUID_temperatureService);
            temperatureCharacteristic = temperatureService.getCharacteristic(UUID_temperatureCharacteristic);
            btGatt.setCharacteristicNotification(temperatureCharacteristic, false);

            //Kosteus
            BluetoothGattService humidityService = gatt.getService(UUID_humidityService);
            humidityCharacteristic = humidityService.getCharacteristic(UUID_humidityCharacteristic);
            btGatt.setCharacteristicNotification(humidityCharacteristic, false);

            //Update
            BluetoothGattService updateService = gatt.getService(UUID_updateService);
            updateCharacteristic = updateService.getCharacteristic(UUID_updateService);


        }


        //Characteristic luku & print toiminto:
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);

            //Ble viesti muunokset byteksi(data) ja floatiksi(value):
            final byte[] data = characteristic.getValue();

            //Valitaan Tallennus kohta ja paikka
            switch (printTarget) {
                case 0: //Led
                    ledStatus = data[0] == (byte) (1);
                    //Log.i("ledStatus", Boolean.toString(ledStatus));
                    break;
                case 1: //Temperature
                    value = ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN).getFloat();
                    temperature = value;
                    printTarget = 0;
                    break;
                case 2: //Humidity
                    value = ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN).getFloat();
                    humidity = value;
                    printTarget = 0;
                    break;
            }

            super.onCharacteristicRead(gatt, characteristic, status);
        }

        //Characteristic kirjoitus toiminto:
        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
        }

        //Notifikaatio muodossa reagoi jos lämpötila tai kosteus arvot muuttuu.
        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            if(btGatt != null){
                updateTemperature();

                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        updateHumidity();
                    }
                }, 500);

                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                temperatureView.setText(temperature + "°C");
                                humidityView.setText(humidity + "%");
                                addToDatabase();
                                warningNotification();
                            }
                        });
                    }
                }, 500);

            }else{
                Log.i("Write", "BLE-UNO device not found");
            }
        }
    };


    //-------- UI-Functiot --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //Led On/Off & notification nappit löytyy onCreate osiosta...

    //Update napin toiminto:
    public void updateFunction(View v) {

        //Estetään muut toiminnot päivityksen aikana ja ilmoitetaan asiasta käyttäjälle
        disconnectButton.setEnabled(false);
        ledButton.setEnabled(false);
        updateButton.setEnabled(false);
        Toast.makeText(getApplicationContext(), "Updating...", LENGTH_SHORT).show();

        //Lähetetään Update "Request"
        Log.i("Update", "Started updateData");
        updateData();

        //Päivitetään Led, lämpötila, kosteus & display 0.5s aikavälein
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.i("Update", "Started updateLed");
                updateLed();
            }
        }, 500);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.i("Update", "Started updateTemperature");
                updateTemperature();
            }
        }, 1000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.i("Update", "Started updateHumidity");
                updateHumidity();
            }
        }, 1500);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ledButton.setChecked(ledStatus);
                temperatureView.setText(temperature + "°C");
                humidityView.setText(humidity + "%");

                disconnectButton.setEnabled(true);
                ledButton.setEnabled(true);
                updateButton.setEnabled(true);
                notificationButton.setEnabled(true);
                warningNotification();
                addToDatabase();
                Log.i("Update", "Done");
            }
        }, 2000);
    }


    //Connect Nappin toiminto:
    public void connectFunction(View v) {
        if (btAdapter.isEnabled()) {
            if (scanStatus) {
                scanStatus = false;
                stopScanning();
                connectButton.setText("Connect");
            } else {
                scanStatus = true;
                startScanning();
                connectButton.setText("Cancel");
            }
        } else {
            Log.i("conFunction", "Starting enableBt");
            enableBt();
        }
    }

    //Disconnet napin toiminto:
    public void disconnectFunction(View v) {
        disconnection();
        Log.i("Connection", "Disconnected");
        Toast.makeText(getApplicationContext(), "Disconnected", LENGTH_SHORT).show();
        connectButton.setText("Connect");
    }

    //View Database nappin toiminto:
    public void viewDatabase(View v) {
        Intent viewDBIntent = new Intent(getApplicationContext(), ViewData.class);
        startActivity(viewDBIntent);
    }

    //--------Nappien aliohjelmat-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //Led päälle toimintio:
    public void ledOn() {
        if (btGatt != null) {
            byte[] value = new byte[1];
            value[0] = (byte) (1);
            ledSwitchCharacteristic.setValue(value);
            btGatt.writeCharacteristic(ledSwitchCharacteristic);
            Log.i("Led", "Turned on");
        } else {
            Log.i("Write", "BLE-UNO device not found");
        }
    }

    //Led sammutus toiminto:
    public void ledOff() {
        if (btGatt != null) {
            byte[] value = new byte[1];
            value[0] = (byte) (0);
            ledSwitchCharacteristic.setValue(value);
            btGatt.writeCharacteristic(ledSwitchCharacteristic);
            Log.i("Led", "Turned off");
        } else {
            Log.i("Write", "BLE-UNO device not found");
        }
    }

    //Scannauksen alotus toiminto:
    public void startScanning() {
        btScanner = btAdapter.getBluetoothLeScanner();
        btScanner.startScan(leScanCallback);
    }

    //Scannauksen lopetus toiminto:
    public void stopScanning() {
        btScanner.stopScan(leScanCallback);
    }

    //BLE Yhdistys toiminto:
    public void connection() {
        if (deviceName.equals("BLE-UNO")) {
            Log.i("Connection", "BLE-UNO found");
            BluetoothDevice device = btAdapter.getRemoteDevice(adress);
            device.connectGatt(getApplicationContext(), false, connectecallabck);
            //Näytetään piilotetut toiminnot ja poistetaan led nappi käytöstä
            displayOn();
            ledButton.setEnabled(false);
            notificationButton.setEnabled(false);
            //Ilmoitetaan, että yhteyden otto onnistui ja käyttäjän tulee painaa updatenappia saadakseen ominaisuudet käyttöön
            Toast.makeText(getApplicationContext(), "Connected. Please press update-button to use services.", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "device not found", LENGTH_SHORT).show();
            Log.i("Connection", "BLE-UNO device not found !");
        }
    }

    //BLE Yhteyden katkomistoiminto:
    public void disconnection() {
        if (deviceName.equals("BLE-UNO") && btGatt != null) {
            Log.i("BLE", "Starting Disconnect Process...");
            btGatt.disconnect();
            if (btGatt == null) {
                return;
            }
            Log.w("BLE", "mBluetoothGatt closed");
            adress = null;
            Log.i("BLE", "Starting Close Process...");
            btGatt.close();
            btGatt = null;
            displayOff();
        } else {
            displayOff();
            Log.i("Disconnect", "BLE-UNO device not found");
        }
    }

    //-----Display On & Off-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //Muutetaan näkymättömät elementit näkyviksi (Connected)
    public void displayOn() {
        ledButton.setVisibility(View.VISIBLE);
        connectButton.setVisibility(View.GONE);
        disconnectButton.setVisibility(View.VISIBLE);
        updateButton.setVisibility(View.VISIBLE);
        notificationButton.setVisibility(View.VISIBLE);
        databaseButton.setVisibility(View.VISIBLE);
        temperatureView.setVisibility(View.VISIBLE);
        humidityView.setVisibility(View.VISIBLE);

    }

    //Muutetaan näkyvät elementit näkymättömäksi (Disconnected)
    public void displayOff() {
        ledButton.setVisibility(View.GONE);
        connectButton.setVisibility(View.VISIBLE);
        disconnectButton.setVisibility(View.GONE);
        updateButton.setVisibility(View.GONE);
        notificationButton.setVisibility(View.GONE);
        databaseButton.setVisibility(View.GONE);
        temperatureView.setVisibility(View.GONE);
        humidityView.setVisibility(View.GONE);
    }



    //----Update aliohjelmat-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //Pyydetään datan päivitystä
    public void updateData() {
        if (btGatt != null) {
            byte[] value = new byte[1];
            value[0] = (byte) (0);
            updateCharacteristic.setValue(value);
            btGatt.writeCharacteristic(updateCharacteristic);
        } else {
            Log.i("Write", "BLE-UNO device not found");
        }
    }


    //Päivitetään puhelimen led on/off nappi
    public void updateLed() {
        if (btGatt != null) {
            printTarget = 0;
            btGatt.readCharacteristic(ledSwitchCharacteristic);
        } else {
            Log.i("Write", "BLE-UNO device not found");
        }
    }

    //Päivitetään Lämpötila
    public void updateTemperature() {
        if (btGatt != null) {
            printTarget = 1;
            btGatt.readCharacteristic(temperatureCharacteristic);
        } else {
            Log.i("Write", "BLE-UNO device not found");
        }
    }

    //Päivitetään Kosteus
    public void updateHumidity() {
        if (btGatt != null) {
            printTarget = 2;
            btGatt.readCharacteristic(humidityCharacteristic);
        } else {
            Log.i("Write", "BLE-UNO device not found");
        }
    }

    //------Notification Aliohjelmat----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //Aliohjelma kun Notifikaationappi menee päälle
    public void notificationOn() {
        //Laitetaan update notification muotoon (Data Source)
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (btGatt != null) {
                    byte[] value = new byte[1];
                    value[0] = (byte) (1);
                    updateCharacteristic.setValue(value);
                    btGatt.writeCharacteristic(updateCharacteristic);
                }
            }
        }, 500);

        //Kytketään notificaatiot päälle lämpötilalle ja kosteudelle
        btGatt.setCharacteristicNotification(temperatureCharacteristic, true);
        btGatt.setCharacteristicNotification(humidityCharacteristic, true);

        //Descriptereiten Notification write
        UUID uuid = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
        BluetoothGattDescriptor temperatureDescriptor = temperatureCharacteristic.getDescriptor(uuid);
        temperatureDescriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        btGatt.writeDescriptor(temperatureDescriptor);

        BluetoothGattDescriptor humidityDescriptor = temperatureCharacteristic.getDescriptor(uuid);
        humidityDescriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        btGatt.writeDescriptor(humidityDescriptor);

        //Laitetaan turhat napit pois päältä. User Experience
        updateButton.setEnabled(false);
        disconnectButton.setEnabled(false);
        databaseButton.setEnabled(false);
    }

    //Aliohjelma kun Notifikaationappi menee pois päältä
    public void notificationOff() {
        if (btGatt != null) {
            byte[] value = new byte[1];
            value[0] = (byte) (3);
            updateCharacteristic.setValue(value);
            btGatt.writeCharacteristic(updateCharacteristic);
            Log.i("Notification", "UpdateCha = 3");

        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                btGatt.setCharacteristicNotification(temperatureCharacteristic, false);
                btGatt.setCharacteristicNotification(humidityCharacteristic, false);
            }
        }, 500);
        updateButton.setEnabled(true);
        disconnectButton.setEnabled(true);
        databaseButton.setEnabled(true);
    }
    private void warningNotification() {
        if(temperature>35.0){
            Toast.makeText(getApplicationContext(), "It's getting too hot!", LENGTH_SHORT).show(); }
        if(temperature<-20.0){
            Toast.makeText(getApplicationContext(), "It's getting too cold!", LENGTH_SHORT).show(); }
        if(humidity>70.0){
            Toast.makeText(getApplicationContext(), "It's getting too wet!", LENGTH_SHORT).show(); }
        if(humidity<30.0){
            Toast.makeText(getApplicationContext(), "It's getting too dry!", LENGTH_SHORT).show(); }
    }

    //-------------FireBase-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    private void addToDatabase(){
        //Lisätään tiedot classiin
        dataInsert.setTemperature(temperature);
        dataInsert.setHumidity(humidity);
        dataInsert.setLed(ledStatus);
        Long timeStamp = System.currentTimeMillis()/1000;
        dataInsert.setTimestamp(timeStamp);

        //Luodaan päiväys string:
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat= new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String dateString = simpleDateFormat.format(calendar.getTime());
        dataInsert.setDate(dateString);

        //Pusketaan tiedot databaseen:
        mDatabase.push().setValue(dataInsert);
        Log.i("Database", "data inserted successfully");
    }
}