package com.example.bleclient;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class ViewData extends AppCompatActivity {

    //Apumuuttujat:
    int counter = 1;

    //UI-Napit & Chart:
    Button viewTemperatureChart, viewHumidityChart,viewLedChart;
    LineChart lineChart;

    //Chart Asetukset:
    int[] colorTemplate = ColorTemplate.MATERIAL_COLORS;
    int textColor = Color.BLACK;
    float textSize = 16f;
    int animationTime = 2000;

    //Chart Setup:
    final ArrayList<String> xAxisLabel = new ArrayList<>();
    final ArrayList<Entry> temperatureMeasurements = new ArrayList<>();
    final ArrayList<Entry> humidityMeasurements = new ArrayList<>();
    final ArrayList<Entry> ledMeasurements = new ArrayList<>();



    //Käynnistys functio:
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_data);

        //UI-Button & Chart Setup
        viewTemperatureChart = findViewById(R.id.viewTemperature);
        viewHumidityChart = findViewById(R.id.viewHumidity);
        viewLedChart = findViewById(R.id.viewLed);
        lineChart = findViewById(R.id.chart);

        //Piiloitetaan tyhjä chart
        lineChart.setVisibility(View.GONE);

        //Firebase Setup:
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("/Measurements/");

        //Luetaan Firebasen sisältö:
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot childSnapShot : dataSnapshot.getChildren()) {
                    String dateData = childSnapShot.child("date").getValue().toString();
                    String temperatureData = childSnapShot.child("temperature").getValue().toString();
                    String humidityData = childSnapShot.child("humidity").getValue().toString();
                    String ledData = childSnapShot.child("led").getValue().toString();

                    //Muutetaan lämpötila & kosteus floatiksi:
                    float temperatureFloat = Float.parseFloat(temperatureData);
                    float humidityFloat = Float.parseFloat(humidityData);

                    //Täytetään Listat datalla:
                    temperatureMeasurements.add(new Entry(counter, temperatureFloat));
                    humidityMeasurements.add(new Entry(counter,humidityFloat));
                    xAxisLabel.add(dateData);
                    if(ledData.equals("true")){
                        ledMeasurements.add(new Entry(counter, 1));
                    }
                    else {
                        ledMeasurements.add(new Entry(counter, 0));
                    }
                    counter++;
                }
                counter = 0;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });

    }

    //UI-Lämpötilanappi
    public void viewTemperatureChart(View v){
        //Näytettän chart
        if(lineChart.getVisibility()==View.GONE){
            lineChart.setVisibility(View.VISIBLE); }

        //Chart materiaalin & ulkonäön asettaminen
        LineDataSet lineDataSet = new LineDataSet(temperatureMeasurements,"Temperature");
        lineChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xAxisLabel));    //X-Axselin date lisäys
        lineDataSet.setColors(colorTemplate);
        lineDataSet.setValueTextColor(textColor);
        lineDataSet.setValueTextSize(textSize);

        //Viivan asetuksien ottaminen datasetistä
        LineData lineData = new LineData(lineDataSet);

        //"Asetetaan" viiva ja nimetään chart
        lineChart.setData(lineData);
        lineChart.getDescription().setText("Temperature Chart");

        //Animaatioiten pituuden määritys
        lineChart.animateY(animationTime);
        lineChart.animateX(animationTime);
    }

    //UI-Kosteusnappi
    public void viewHumidityChart(View v){
        //Näytettän chart
        if(lineChart.getVisibility()==View.GONE){
            lineChart.setVisibility(View.VISIBLE); }

        //Chart materiaalin & ulkonäön asettaminen
        LineDataSet lineDataSet = new LineDataSet(humidityMeasurements,"Humidity");
        lineChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xAxisLabel));    //X-Axselin date lisäys
        lineDataSet.setColors(colorTemplate);
        lineDataSet.setValueTextColor(textColor);
        lineDataSet.setValueTextSize(textSize);

        //Viivan asetuksien ottaminen datasetistä
        LineData lineData = new LineData(lineDataSet);

        //"Asetetaan" viiva ja nimetään chart
        lineChart.setData(lineData);
        lineChart.getDescription().setText("Humidity Chart");

        //Animaatioiten pituuden määritys
        lineChart.animateY(animationTime);
        lineChart.animateX(animationTime);
    }

    //UI-Lednappi
    public void viewLedChart(View v){
        //Näytettän chart
        if(lineChart.getVisibility()==View.GONE){
            lineChart.setVisibility(View.VISIBLE); }

        //Chart materiaalin & ulkonäön asettaminen
        LineDataSet lineDataSet = new LineDataSet(ledMeasurements,"Led Status");
        lineChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xAxisLabel));    //X-Axselin date lisäys
        lineDataSet.setColors(colorTemplate);
        lineDataSet.setValueTextColor(textColor);
        lineDataSet.setValueTextSize(textSize);

        //Viivan asetuksien ottaminen datasetistä
        LineData lineData = new LineData(lineDataSet);

        //"Asetetaan" viiva ja nimetään chart
        lineChart.setData(lineData);
        lineChart.getDescription().setText("Led Status Chart");

        //Animaatioiten pituuden määritys
        lineChart.animateY(animationTime);
        lineChart.animateX(animationTime);
    }
}