package com.example.bleclient;

public class DataInsert {
    private Float Temperature;
    private Float Humidity;
    private Boolean Led;
    private Long Timestamp;
    private String Date;

    public DataInsert() {
    }

    public Float getTemperature() {
        return Temperature;
    }

    public void setTemperature(Float temperature) {
        Temperature = temperature;
    }

    public Float getHumidity() {
        return Humidity;
    }

    public void setHumidity(Float humidity) {
        Humidity = humidity;
    }

    public Boolean getLed() {
        return Led;
    }

    public void setLed(Boolean led) {
        Led = led;
    }

    public Long getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(Long timestamp) {
        Timestamp = timestamp;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}

