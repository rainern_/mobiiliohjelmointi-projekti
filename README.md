 # Mobiiliprojekti Suomeksi
 ## Tavoitteet/Idea
 Tavoitteena oli luoda projecti, jolla voidaan lukea arvoja(lämpötila, kosteus & ledin tila) arduinolta puhelimella ja ohjata lediä. Tämän tulisi tapahtua Bluetooth Low Energy yhteydellä. Tämän lisäksi halusin, että luetut arvot lähetettäisiin Firebasen Realtime Databaseen, josta arvot tai tietyt arvot voidaisiin lukea heposti ymmärettävässä tai hienossa muodossa. Koko projecti tehtäisiin "tyhjästä".
 
 ## Toteutus
 ### BLE-Yhteys
 Yhteys tapahtuu automaattisesti, mutta aruino laitteen on käytettävä projectissa annetua koodia toimiakseen.
 Connect nappi scannaa laitteita, jos se löytää oikean, niin se yhdistää automaattisesti. Scannauksen voi kuitenkin perua painamalla nappia uudestaan, jos se ei ole löytänyt etsimäänsä laitetta.
 
 ### Ominaisuudet
 - Led on/off
 - Manuaalinen update
 - Notification service / Automaattinen lämpötilan & kosteuden luku (x ajan kulttua, koska arduino määrittää milloin arvoa vaihdetaan)
 - Näytä/lue databasen tiettyjä arvoja
 
 ## Lopputulos
 Projekti tekee kaiken mitä siltä halusin, mutta siinä on parannettavaa...
 
 
 ## Parannettavaa
 - Icon on nopeasti tehty, joten ruma kuin mikä...
 - Connect napin, jälkeen voisi olla delay: Jos yhdistää ja painaa heti, kun mahdollista update nappia -->Ohjelma voi kaatua, jos BLE ei ole saanut yhtettä heti kuntoon...
 - Chartien x-akselin info voisi olla paremmin, saadakseen tarkempia tulosksia tai vain paremman näköinen
 - Tähän alkuun voisi lisätä login osan, että kukaan ei pääse asiattomasti muokkaamaan databasea. 
 - Luvat ja ominaisuuksien automaattinen päällelaitto voisi toimia paremmin.
 
 ## Huomautettavaa 
 - Arduino koodin notification muodossa arduino lähettää arvoa aika kovaan tahtiin, joka ei ole realistinen normaaliin käyttöön. Tämä on nopeaksi laitettu vain demo mielessä...
 - Android sovellus kysyy lupia, mutta ei välttämättä käynnistä paikannusta --> joudut mahdollisesti laitaamaan sen päälle manuaalisesti, että ohjelma toimii oikein.
 - Projekti on nyt julkinen --> Google Service File on poistettu...


# Mobile Project In English
 ## End Goals
Projects end goal was to create a system where you can read values(Temperature, humidity & LED status) from arduino using your mobile phone and contorl arduinos led as well. This all needs to happen via Bluetooth Low Energy(BLE) connection. In addition I wanted that values that phone reads are sent to Firebases Realtime Database where the data can be read in easy to understand or fancier format. The whole project was made from ground up.
 
 ## Execution
 ### BLE-Connection
The connection happens automatically but it does not work if you do not use the same arduino code as in this project.
The connect button scans devices around the host device and if it finds the right device it connects. You can cancel the scan by pressing the button again if it does not find the right device.
 
 ### Features
 - Led on/off
 - Manual update
 - Notification service / Read temperature and humidity automatically (x time spent because arduino dictates when these values are changed)
 - Dispalys and reads values from database
 
 ## End Result
 Project does everything that I wanted but it can be still improved on...
 
 
 ## Improvements
 - Icon was made in a hurry so it is ugly as sin
 - After using the connect button there could be a delay: If you connect and press the update button as soon as possible the program can crash if the BLE connection is not fully initialized...
 - Charts x axel info could be displayed better to get more accurate results and improve visuals.
 - At the start of the program there could be a login section to restrict access to the database.
 - Permissions and automatic turn on features function could work better.
 
 ## Remarks/Notes
 - Arduino codes notification function sends values at a fast pace. This is not realistic use of this program and is only doing so for demonstration purposes.
 - The android app asks for permissions but might not turn the features on automatically --> you have to turn Bluetooth and location services yourself in order for the app to work as intended.
 - The project is public --> Google Services file is removed...

